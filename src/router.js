import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import OidcCallback from './views/OidcCallback.vue';
//import OidcCallbackError from './views/OidcCallbackError.vue';
import { vuexOidcCreateRouterMiddleware } from 'vuex-oidc';
import store from '@/store';
//import Logout from './views/Logout.vue';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {
                isPublic: true
            }
        },
        {
            path: '/protected',
            name: 'protected',
            component: () =>
                import(/* webpackChunkName: "protected" */ './views/Protected.vue')
        },
        {
            path: '/logout',
            name: 'logout',
            //component: Logout
            component: () =>
                import(/* webpackChunkName: "logout" */ './views/Logout.vue'),
            meta: {
                isPublic: true
            }
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () =>
                import(/* webpackChunkName: "about" */ './views/About.vue')
        },
        {
            path: '/userSettings',
            name: 'userSettings',
            component: () =>
                import(/* webpackChunkName: "userSettings" */ './views/UserSettings.vue')
        },
        {
            path: '/oidc-callback', // Needs to match redirect_uri in you oidcSettings
            name: 'oidcCallback',
            component: OidcCallback,
            meta: {
                isOidcCallback: true,
                isPublic: true
            }
        }
        //{
        //    path: '/oidc-callback-error', // Needs to match redirect_uri in you oidcSettings
        //    name: 'oidcCallbackError',
        //    component: OidcCallbackError,
        //    meta: {
        //        isPublic: true
        //    }
        //}
    ]
});

router.beforeEach(vuexOidcCreateRouterMiddleware(store));

export default router;
