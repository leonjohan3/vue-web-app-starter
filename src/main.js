import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VTooltip from 'v-tooltip';
import '@/assets/global.css';

Vue.config.productionTip = false;
Vue.use(VTooltip);
Vue.use(VueAxios, axios);
VTooltip.enabled = window.innerWidth > 768;

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app');
