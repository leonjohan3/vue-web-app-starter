import Vue from 'vue';
import Vuex from 'vuex';
import { vuexOidcCreateStoreModule } from 'vuex-oidc';
import { oidcSettings } from './config/vuex-oidc';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        oidcStore: vuexOidcCreateStoreModule(
            oidcSettings,
            // Optional OIDC store settings
            {
                namespaced: false,
                routeBase: process.env.BASE_URL
                //dispatchEventsOnWindow: false
            }
            // Optional OIDC event listeners
            //            {
            //                userLoaded: user => console.log('OIDC user is loaded:', user),
            //                userUnloaded: () => console.log('OIDC user is unloaded'),
            //                accessTokenExpiring: () =>
            //                    console.log('Access token will expire'),
            //                accessTokenExpired: () =>
            //                    console.log('Access token did expire'),
            //                silentRenewError: () => console.log('OIDC user is unloaded'),
            //                userSignedOut: () => console.log('OIDC user is signed out')
            //            }
        )
    },
    state: {
        //drawerOpen: false,
        //showTooltips: false,
        //userName: 'anonymous'
    },
    mutations: {
        //flipDrawer(state) {
        //    state.drawerOpen = !state.drawerOpen;
        //},
        //setDrawerOpen(state, newValue) {
        //    state.drawerOpen = newValue;
        //},
        //setIsSignedInTrue(state) {
        //    state.isSignedIn = true;
        //},
        //setIsSignedInFalse(state) {
        //    state.isSignedIn = false;
        //        },
        //        setUserName(state, newValue) {
        //            state.userName = newValue;
        //        }
    },
    actions: {
        //        logout({ state, commit }) {
        //            state.mgr.signOut().then(
        //                () => {
        //                    commit('setUserName', '');
        //                    commit('setIsSignedInFalse');
        //                },
        //                error => {
        //                    console.log(error);
        //                }
        //            );
        //        },
        //        login({ state, commit }) {
        //            state.mgr.getUser().then(
        //                user => {
        //                    commit('setUserName', user.profile.name);
        //                    commit('setIsSignedInTrue');
        //                },
        //                error => {
        //                    console.log(error);
        //                }
        //            );
        //        },
        //        setUserName({ state, commit }) {
        //            state.mgr.getProfile().then(
        //                profile => {
        //                    commit('setUserName', profile.name);
        //                    //commit('setIsSignedInTrue');
        //                },
        //                error => {
        //                    console.log(error);
        //                }
        //            );
        //        },
        //        getSignedIn({ state, commit }) {
        //            state.mgr.getSignedIn().then(
        //                isSignedIn => {
        //                    if (isSignedIn) {
        //                        commit('setIsSignedInTrue');
        //                    } else {
        //                        commit('setIsSignedInFalse');
        //                    }
        //                },
        //                error => {
        //                    console.log(error);
        //                }
        //            );
    }
    //    }
});
