export const oidcSettings = {
    authority: 'https://accounts.google.com',
    automaticSilentRenew: true,
    clientId: process.env.VUE_APP_CRED_PROVIDER_CLIENT_ID,
    redirectUri: `${process.env.VUE_APP_AUTH_CALLBACK_BASE}${
        process.env.BASE_URL
    }callback.html`,
    responseType: 'id_token token',
    scope: 'openid profile email',
    silentRedirectUri: `${process.env.VUE_APP_AUTH_CALLBACK_BASE}${
        process.env.BASE_URL
    }silent-renew-oidc.html`
};
