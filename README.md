# vue-web-app-starter

## Overview
This is a [web app](<https://en.wikipedia.org/wiki/Web_application>) starter (aka a scaffold, boilerplate or seed). It supports [authentication](<https://en.wikipedia.org/wiki/Authentication>) (authenticating using Google as an example) and different roles to demonstrate [authorization](<https://en.wikipedia.org/wiki/Authorization>). It also supports plugging in different business [logos](<https://en.wikipedia.org/wiki/Logo>) and [brand icons](<https://en.wikipedia.org/wiki/Icon_(computing)#Brand_icons_for_commercial_software>). It allows the customization of the [theme](<https://en.wikipedia.org/wiki/Theme_(computing)>). The application has basic [menus](<https://en.wikipedia.org/wiki/Menu_(computing)>), a basic [input form](<https://en.wikipedia.org/wiki/Form_(HTML)>) and a basic [dashboard](<https://en.wikipedia.org/wiki/Dashboard_(business)>). The menus, form and dashboard can easily be changed and replaced by following the instructions in this README.

## Installing, running and building
To deploy the application locally, clone the repository, and then run the commands below:

    npm install
    npm run serve
    npm run build

## Changing the theme
* [To change the color theme of the application](<https://vuetifyjs.com/en/customization/theme>)
* [Adjusting the colors of UI elements using classes](<https://vuetifyjs.com/en/styles/colors>)

## Changing the authentication provider
* Edit the [vuex-oidc.js](<https://bitbucket.org/leonjohan3/vue-web-app-starter/src/develop/src/config/vuex-oidc.js>) file in the config folder.
* The clientId has been externalised and should be added to the .env.local file. For additional information see [Vue environment-variables](<https://cli.vuejs.org/guide/mode-and-env.html#environment-variables>)

## Todo
1.  Externalize the Google oidc client id. [done]
2.  Create a user profile screen showing first name, surname, email, email verified and token expire time.  

## Resources
1.  [Vue CLI Configuration Reference](<https://cli.vuejs.org/config/>)
2.  [README.md Markup Guide](<https://spec.commonmark.org/0.29/>)
3.  [Vue CLI](<https://cli.vuejs.org>)
4.  [Vue Style Guide](<https://vuejs.org/v2/style-guide/>)
5.  [Vuetify](<https://vuetifyjs.com/en/getting-started/quick-start>)
6.  [Google Contacts Vuetify Layout](<https://github.com/vuetifyjs/vuetify/blob/master/packages/docs/src/examples/layouts/google-contacts.vue>)
7.  [Vue Guide](<https://vuejs.org/v2/guide/>)
8.  [OpenID Connect](<https://en.wikipedia.org/wiki/OpenID_Connect>)
9.  [oidc-client](<https://github.com/IdentityModel/oidc-client-js/wiki>)
10. [vuex-oidc](<https://github.com/perarnborg/vuex-oidc/wiki>)
11. [Google Developer Console](<https://console.developers.google.com/apis/credentials?project=mypoc-250308>)
12. [Google OAuth Documentation](<https://developers.google.com/identity/protocols/OAuth2UserAgent>)
13. [12 Factpr App](<http://12factor.net/config>)
14. [v-tooltip](<https://github.com/Akryum/v-tooltip>)
