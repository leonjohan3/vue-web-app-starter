module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/starter/' : '/',
    transpileDependencies: ['vuetify'],
    pages: {
        app: {
            entry: 'src/main.js',
            template: 'public/index.html',
            filename: 'index.html',
            excludeChunks: ['silentrenewoidc', 'oidccallback']
        },
        silentrenewoidc: {
            entry: 'src/silent-renew-oidc.js',
            template: 'public/silent-renew-oidc.html',
            filename: 'silent-renew-oidc.html',
            excludeChunks: ['app', 'oidccallback']
        },
        oidccallback: {
            entry: 'src/oidc-callback.js',
            template: 'public/callback.html',
            filename: 'callback.html',
            excludeChunks: [
                'app',
                'silentrenewoidc',
                'oidccallback',
                'chunk-vendors'
            ]
        }
    }
};
